pub struct AppSysInfo {
    cpu: String,
    name: String,
    os_version: String,
    display_card: Vec<String>,
    camera: Vec<String>,
    bluetooth: Vec<String>,
    voice: Vec<String>,
    microphone: Vec<String>
}

impl AppSysInfo {
    
    pub fn init() {

    }

    pub fn get_computer_name() {
        
    }

    pub fn get_os_version() {

    }

    pub fn get_display_cardinfo_wmic() {

    }

    pub fn get_cpu_info() {

    }

    pub fn get_bluetooth_info() {

    }

    pub fn get_camera_info() {

    }

    pub fn get_micro_phone() {

    }
    
    pub fn get_gpu_info() {

    }

}